import com.commercetools.api.client.ProjectApiRoot;
import com.commercetools.api.defaultconfig.ApiRootBuilder;
import com.commercetools.api.defaultconfig.ServiceRegion;
import io.vrap.rmf.base.client.oauth2.ClientCredentials;

public class Client {

    public static ProjectApiRoot createApiClient() {
        final ProjectApiRoot apiRoot = ApiRootBuilder.of()
                .defaultClient(ClientCredentials.of()
                                .withClientId("I00uzr37BQmiuHcqh497RWWS")
                                .withClientSecret("GzxPEHEp0uXezO3wt2kxmsHC5ZvtMRJw")
                                .build(),
                        ServiceRegion.GCP_EUROPE_WEST1)
                .build("lesson1");

        return apiRoot;
    }
}
