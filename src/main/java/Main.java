import com.commercetools.api.client.ProjectApiRoot;
import com.commercetools.api.models.project.Project;
import com.commercetools.api.models.customer.*;


public class Main {
    public static void main(String[] args) {
        ProjectApiRoot apiRoot = Client.createApiClient();

        // Make a get call to the Project
        Project myProject = apiRoot
                .get()
                .executeBlocking()
                .getBody();

//        CustomerDraft newCustomerDraft = CustomerDraft.builder()
//                .email("java-sdk@example.com")
//                .password("password")
//                .build();
//
//        Customer newCustomer = apiRoot
//                .customers()
//                .post(newCustomerDraft)
//                .executeBlocking()
//                .getBody() // As creating a Customer returns a CustomerSignInResult, .getCustomer() is required to get the new Customer object
//                .getCustomer();
//        System.out.println(newCustomer.getEmail());

        CustomerPagedQueryResponse allCustomers = apiRoot.customers()
                .get()
                .executeBlocking()
                .getBody();

        // Output the Project name
        System.out.println(myProject.getName());
        System.out.println(allCustomers.getResults().toString());

        // Create the payload - a CustomerUpdate - with the current version of the Customer and the update actions.
        CustomerUpdate customerUpdate = CustomerUpdateBuilder.of()
                .version(1L)
                .plusActions(actionBuilder -> actionBuilder
                        .setFirstNameBuilder()
                        .firstName("test"))
                .build();

        // Update a Customer
        Customer updatedCustomer = apiRoot
                .customers()
                .withId("a53b77cf-d65e-435a-91d5-ce2b1359966a")
                .post(customerUpdate)
                .executeBlocking()
                .getBody();

        System.out.println(updatedCustomer.getFirstName());
    }
}